# rtl-sdr-tcp

Creates a rtl-sdr tcp instance on a raspberry for remote accsess

## Build Image

```bash
docker buildx build --platform linux/arm/v7 -t "rtl-sdr-tcp:latest" .
```

## Start Container

```bash
docker run -it --privileged  --device=/dev/ttyAMA0 --name rtl-sdr-tcp -p 1234:1234 rtl-sdr-tcp
```

## Interactive Shell

```bash
docker run -it --privileged  --device=/dev/ttyAMA0 --name rtl-sdr-tcp -p 1234:1234 rtl-sdr-tcp /bin/bash
```