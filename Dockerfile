FROM balenalib/raspberrypi3-debian:latest

RUN apt update && apt upgrade -y 
RUN	sudo apt install apt-utils libusb-1.0-0-dev git build-essential make cmake 
RUN	sudo apt autoremove
RUN mkdir /home/usr
RUN cd /home/usr
RUN	git clone git://github.com/rtlsdrblog/rtl-sdr-blog.git /home/usr/rtl-sdr-blog
RUN	cd /home/usr/rtl-sdr-blog/
RUN ls -a 
RUN	mkdir /home/usr/rtl-sdr-blog/build
RUN cd /home/usr/rtl-sdr-blog/build 
RUN	cmake /home/usr/rtl-sdr-blog/ -DINSTALL_UDEV_RULES=ON 
RUN	sudo make
RUN ls
RUN	sudo make install /home/usr/rtl-sdr-blog/
RUN	sudo cp /home/usr/rtl-sdr-blog/rtl-sdr.rules /etc/udev/rules.d/ 
RUN	sudo ldconfig 
RUN	echo 'blacklist dvb_usb_rtl28xxu' | sudo tee --append /etc/modprobe.d/blacklist-dvb_usb_rtl28xxu.conf

EXPOSE 1234/tcp

CMD rtl_tcp -a 172.17.0.6 -p 1234